#!/bin/bash
TARGET_ORG_ALIAS=$1
TEST_LEVEL=$2
CHECK_ONLY=$3
echo "Running task action with the following params:"
echo "-----------------------------------------------"
echo "TARGET ORG ALIAS: " $TARGET_ORG_ALIAS
echo "TEST MODE: " $TEST_LEVEL
echo "CHECK ONLY VALUE: " $CHECK_ONLY
echo "-----------------------------------------------"
sfdx force:mdapi:deploy -u $TARGET_ORG_ALIAS -d src -g -l $TEST_LEVEL -w 45 $CHECK_ONLY