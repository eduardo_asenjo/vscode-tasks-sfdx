#!/bin/bash
TARGET_ORG_ALIAS=$1
echo "Running retrieve action with the following params:"
echo "-----------------------------------------------"
echo "TARGET ORG: " $TARGET_ORG_ALIAS
echo "-----------------------------------------------"
sfdx force:mdapi:retrieve -u $TARGET_ORG_ALIAS -r ./ -k ./.scripts/metadata_package_to_retrieve.xml -w 45
echo "-----------------------------------------------"
echo "Unzipping package retrieved..."
unzip unpackaged.zip
rm unpackaged.zip
echo "-----------------------------------------------"
echo "Renamming 'unpackaged' folder as 'retrieved-metadata'"
mv unpackaged retrieved-metadata
echo "-----------------------------------------------"
echo "---------------Process Completed---------------"
echo "-----------------------------------------------"